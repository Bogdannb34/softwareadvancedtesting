import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.fail;

public class EmployeeTest {

    @Test
    public void setSalaryShouldThrowException() {
        // given
        Employee employee = new Employee();
        // when
        try {
            employee.setSalary(-100);
            fail("No exception : Salary is positive");
        // then
        } catch (IllegalArgumentException e) {
            e.getMessage();
            assertThat(e.getMessage().equals("Salary cannot be negative!"));
        }
    }

    @Test
    public void setSalaryShouldThrowExceptionOptionOne() {
        //given
        Employee employee = new Employee();
        // when
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> employee.setSalary(-100));
        // then
        assertThat(exception.getMessage().equals("Salary cannot be negative!"));

    }
}
